﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	public WaypointController waypoitControl;
	public float speed = 5;
	private GameObject cam;
	// Use this for initialization
	void Start () {
		cam = GameObject.Find("MainCamera");
	}
	
	// Update is called once per frame
	void Update () {
		float step = speed * Time.deltaTime;
		Vector3 currentWaypointPos = waypoitControl.getCurrentWaypointpos ();
		transform.position = Vector3.MoveTowards (transform.position, currentWaypointPos, step);
		cam.transform.position = transform.position - Vector3.forward * 10f;
		cam.transform.LookAt (transform.position); 	
	}
}
