﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {
	public float rotation = 5f;
	// Use this for initialization

	void Start () {
		transform.Rotate (new Vector3 (rotation * Time.deltaTime, 0, 0));
		Debug.Log ("Rotating From Start Evet");
	}

	// Update is called once per frame
	void Update () {
		transform.Rotate (new Vector3 (0, 0, rotation * Time.deltaTime));
		Debug.Log ("Rotating From Update Event");
	}
}
