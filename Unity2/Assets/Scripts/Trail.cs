﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trail : MonoBehaviour {

	[Range(1,50)]
	public float myWith = 5;
	[Range(1,50)]
	public float myendWith = 2;
	// Update is called once per frame
	void Update () {
		GetComponent<TrailRenderer> ().time = Random.Range (1, 100);
		GetComponent<TrailRenderer> ().startWidth = myWith;
		GetComponent<TrailRenderer> ().endWidth = myendWith;
	}
}
