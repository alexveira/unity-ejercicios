﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColoChanger : MonoBehaviour {
	Renderer myrenderer;
	public bool randomColormode;
	public Color mycolor;
	bool changeColor;
	// Use this for initialization
	void Start () {
		myrenderer = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
		if (randomColormode) {
			mycolor = new Color32 ((byte)Random.Range (0, 255), (byte)Random.Range (0, 255), (byte)Random.Range (0, 255), 1);
		} else {
			mycolor = Color.red;
		}
		if (Input.GetButton ("Color")) {
			if (randomColormode) {
				if (changeColor) {
					myrenderer.material.SetColor ("_Color", mycolor);
					changeColor = false;
				}
			} else {
				myrenderer.material.SetColor ("_Color", mycolor);
			}
		} else {
			changeColor = true;
			myrenderer.material.SetColor ("_Color", Color.white);
		}
	}
}
