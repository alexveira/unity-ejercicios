﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scale : MonoBehaviour {
	public float scaleUnits = 5f;
	// Use this for initialization

	void Start () {
		transform.localScale = new Vector3 (transform.localScale.x + scaleUnits * Time.deltaTime, 1, 1);
		Debug.Log ("Rotating From Start Evet");
	}

	// Update is called once per frame
	void Update () {
		transform.localScale = new Vector3 (transform.localScale.x + scaleUnits * Time.deltaTime, 1, 1);
		Debug.Log ("Rotating From Update Event");
	}
}
