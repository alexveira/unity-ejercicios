using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerMovemet : MonoBehaviour {
	public GameObject OrbitObject;
	[Range(0.5f, 3.0f)]
	public float spawnTime;
	[Range(1f, 10f)]
	public float scale;
	[Range(0f, 50f)]
	public float InitialImpulse;
	public Color prefabColor;
	public GameObject prefabToSpawn;
	Transform center;
	Rigidbody prefabRb;
	Renderer myrenderer;
	GameObject objAux;
	public Vector3 axis = Vector3.up;
	float timecounter;
	public Vector3 desiredPosition;
	public float radius;
	public float radiusSpeed = 0.5f;
	public float rotationSpeed = 80.0f;
	// Use this for initialization
	void Start () {
		center = OrbitObject.transform;
		transform.position = (transform.position - center.position).normalized * radius + center.position;
	}
	// Update is called once per frame
	void Update () {
		transform.RotateAround (center.position, axis, rotationSpeed * Time.deltaTime);
		desiredPosition = (transform.position - center.position).normalized * radius + center.position;
		transform.position = Vector3.MoveTowards(transform.position, desiredPosition, Time.deltaTime * radiusSpeed);
		timecounter += Time.deltaTime;
		if(timecounter > spawnTime){
			SpawnPrefab();
			timecounter = 0f;
		}
	}	
	void SpawnPrefab(){
		objAux = Instantiate (prefabToSpawn, transform.position, Quaternion.identity) as GameObject;
		myrenderer = objAux.GetComponent<Renderer> ();
		myrenderer.material.SetColor ("_Color", prefabColor);
		objAux.transform.localScale = new Vector3 (Random.Range (1f, scale), Random.Range (1f, scale), Random.Range (1f, scale));
		prefabRb = objAux.GetComponent<Rigidbody> ();
		prefabRb.AddForce (new Vector3(Random.Range(-InitialImpulse,InitialImpulse),
									   Random.Range(-InitialImpulse,InitialImpulse),
			                           Random.Range(-InitialImpulse,InitialImpulse)), ForceMode.Impulse);
	}
}
