﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
	private float hInput, vInput, mousex, mousey, rotationx, rotationy;
	public float speed;
	private Vector3 movement;
	Rigidbody myrb;
	Quaternion rotation;
	// Use this for initialization
	void Start () {
		myrb = gameObject.GetComponent<Rigidbody> ();
	}
	// Update is called once per frame
	void Update () {
		mousex = Input.GetAxis("Mouse X");
		mousey = Input.GetAxis("Mouse Y");
		hInput = Input.GetAxisRaw ("Horizontal");
		vInput = Input.GetAxisRaw ("Vertical");
		//movement = new Vector3 (hInput, 0.0f, vInput);
		movement = (transform.forward * vInput) + (transform.right * hInput);
		rotationx += mousex;
		rotationy += mousey;
	}
	void FixedUpdate () {
		myrb.velocity = movement* speed;
		transform.eulerAngles = new Vector3 (-rotationy,rotationx,0) * 5f;
	}
	void OnCollisionEnter(Collision col){
		if (col.gameObject.tag == ("Enemy")) {
			Destroy (gameObject);
		}
	}
}
