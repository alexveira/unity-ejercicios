﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayShoot : MonoBehaviour {
	Camera mycamera;
	// Use this for initialization
	void Start () {
		mycamera = gameObject.GetComponent<Camera> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void FixedUpdate () {
		Ray ray = mycamera.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
		if (Input.GetButtonDown ("Fire1")) {
			if (Physics.Raycast (ray, out hit, 100)) {
				//if (Physics.Raycast (transform.position,transform.forward, out hit, 100)) {
				//Debug.DrawLine (ray.origin, hit.point);
				if (hit.collider.gameObject.tag == ("Enemy") /*&& Input.GetButtonDown("Fire1")*/) {
					Destroy (hit.collider.gameObject);
				}
			}
		}
	}
}
